/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class Registro {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoDiario;
    private int horas;

    public Registro() {
        this.numDocente = 0;
        this.nombre= "" ;
        this.domicilio="";
        this.nivel=0;
        this.pagoDiario=0.0f;
        this.horas=0;
    }

    public Registro(int numDocente, String nombre, String domicilio, int nivel, float pagoDiario, int horas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoDiario = pagoDiario;
        this.horas = horas;
    }
    
    public Registro(Registro x){
        this.numDocente=x.numDocente;
        this.nombre=x.nombre;
        this.domicilio=x.domicilio;
        this.nivel=x.nivel;
        this.pagoDiario=x.pagoDiario;
        this.horas=x.horas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }
    
    public float calcularPago(){
        float incremento =0.0f;
        float pago = 0.0f;
        if(this.nivel==0){
            incremento = (float) (this.pagoDiario * 1.30);
        }
        if(this.nivel==1){
            incremento = (float) (this.pagoDiario * 1.50);
        }
        if(this.nivel==2){
            incremento = (float) (this.pagoDiario * 2);
        }
        pago = incremento * this.horas;
        return pago;
    }
    public float calcularBono(int hijos){
        float bono = 0.0f;
        if(hijos == 1 || hijos == 2){
            bono= (float) (this.calcularPago()* 0.05);
        }
        if(hijos>=3 && hijos<=5){
            bono= (float) (this.calcularPago()* 0.1);
        }
        if(hijos>5){
            bono= (float) (this.calcularPago()* 0.2);
        }
        return bono;
    }
    public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto= (float) (this.calcularPago()*0.16);
        return impuesto;
    }
    public float calcularTotalPagar(int hijos){
        float total= 0.0f;
        total= this.calcularPago()+ this.calcularBono(hijos)-this.calcularImpuesto();
        return total;
    }
}