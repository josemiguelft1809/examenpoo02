
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Figueroa
 */
public class JintRegistro extends javax.swing.JInternalFrame {

    /**
     * Creates new form JintRegistro
     */
    public JintRegistro() {
        initComponents();
        this.resize(1100, 850);
        this.deshabilitar();
    }
    private int hijos;

    public int getHijos() {
        return hijos;
    }

    public void setHijos(int hijos) {
        this.hijos = hijos;
    }
    
    public void deshabilitar(){
        this.txtNumDocente.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtDomicilio.setEnabled(false);
        this.txtPagoDiario.setEnabled(false);
        this.txtHorasTrabajadas.setEnabled(false);
        this.cmbNivel.setEnabled(false);
        this.txtPagoHora.setEnabled(false);
        this.txtPagoBono.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtTotalPagar1.setEnabled(false);
        this.txtHijos.setEnabled(false);
        
        this.btnMostrar.setEnabled(false);
        this.btnGuardar.setEnabled(false);

    }
    
    public void habilitar(){
        this.txtNumDocente.setEnabled(true);
        this.txtNombre.setEnabled(true);
        this.txtDomicilio.setEnabled(true);
        this.txtPagoDiario.setEnabled(true);
        this.txtHorasTrabajadas.setEnabled(true);
        this.cmbNivel.setEnabled(true);
        this.txtHijos.setEnabled(true);
        
        this.btnMostrar.setEnabled(true);
        this.btnGuardar.setEnabled(true);
    }
    
    public void limpiar(){
        this.txtNumDocente.setText("");
        this.txtNombre.setText("");
        this.txtDomicilio.setText("");
        this.txtPagoDiario.setText("");
        this.txtHorasTrabajadas.setText("");
        this.txtPagoHora.setText("");
        this.txtHijos.setText("");
        this.txtPagoBono.setText("");
        this.txtImpuesto.setText("");
        this.txtTotalPagar1.setText("");
        
        this.cmbNivel.setSelectedIndex(0);
        
        this.txtNumDocente.requestFocus();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtNumDocente = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        txtPagoDiario = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtHorasTrabajadas = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtPagoBono = new javax.swing.JTextField();
        txtPagoHora = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtTotalPagar1 = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtHijos = new javax.swing.JTextField();
        btnCerrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        cmbNivel = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(204, 204, 255));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Registro");
        getContentPane().setLayout(null);

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Nombre: ");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(68, 110, 82, 22);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("No. Docente: ");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(40, 50, 120, 22);

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Domicilio: ");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(60, 170, 100, 40);

        jLabel7.setBackground(new java.awt.Color(255, 255, 255));
        jLabel7.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("      Pago Diario:");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(10, 310, 150, 40);

        txtNumDocente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumDocenteActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumDocente);
        txtNumDocente.setBounds(210, 40, 350, 40);

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombre);
        txtNombre.setBounds(210, 100, 350, 40);

        txtDomicilio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDomicilioActionPerformed(evt);
            }
        });
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(210, 170, 350, 40);

        txtPagoDiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoDiarioActionPerformed(evt);
            }
        });
        getContentPane().add(txtPagoDiario);
        txtPagoDiario.setBounds(210, 310, 350, 40);

        jLabel9.setBackground(new java.awt.Color(255, 255, 255));
        jLabel9.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("           Nivel: ");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(40, 240, 120, 40);

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Horas Trabajadas:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(2, 370, 158, 40);

        txtHorasTrabajadas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHorasTrabajadasActionPerformed(evt);
            }
        });
        getContentPane().add(txtHorasTrabajadas);
        txtHorasTrabajadas.setBounds(210, 370, 350, 40);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel10.setText("Pago por bono: ");

        jLabel11.setBackground(new java.awt.Color(255, 255, 255));
        jLabel11.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel11.setText("Pago por horas impartidas: ");

        txtPagoBono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoBonoActionPerformed(evt);
            }
        });

        txtPagoHora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoHoraActionPerformed(evt);
            }
        });

        jLabel12.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel12.setText("Total a pagar: ");

        jLabel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel13.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel13.setText("Descuento por Impuesto: ");

        txtTotalPagar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalPagar1ActionPerformed(evt);
            }
        });

        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel14.setText("            Hijos: ");

        txtHijos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHijosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 508, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(30, 30, 30)
                            .addComponent(jLabel11)
                            .addGap(19, 19, 19)
                            .addComponent(txtPagoHora, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel14)
                            .addGap(14, 14, 14)
                            .addComponent(txtHijos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(20, 20, 20)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(140, 140, 140)
                                    .addComponent(txtPagoBono, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(30, 30, 30)
                            .addComponent(jLabel13)
                            .addGap(13, 13, 13)
                            .addComponent(txtImpuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addComponent(jLabel12)
                            .addGap(6, 6, 6)
                            .addComponent(txtTotalPagar1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 261, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtPagoHora, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(20, 20, 20)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtHijos, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtPagoBono, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtImpuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(20, 20, 20)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtTotalPagar1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(120, 440, 520, 290);

        btnCerrar.setBackground(new java.awt.Color(0, 0, 0));
        btnCerrar.setFont(new java.awt.Font("Arial Black", 0, 18)); // NOI18N
        btnCerrar.setForeground(new java.awt.Color(255, 255, 255));
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(550, 750, 160, 30);

        btnNuevo.setBackground(new java.awt.Color(0, 0, 0));
        btnNuevo.setFont(new java.awt.Font("Arial Black", 0, 18)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(255, 255, 255));
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(850, 80, 160, 60);

        btnGuardar.setBackground(new java.awt.Color(0, 0, 0));
        btnGuardar.setFont(new java.awt.Font("Arial Black", 0, 18)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(850, 200, 160, 60);

        btnMostrar.setBackground(new java.awt.Color(0, 0, 0));
        btnMostrar.setFont(new java.awt.Font("Arial Black", 0, 18)); // NOI18N
        btnMostrar.setForeground(new java.awt.Color(255, 255, 255));
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(850, 320, 160, 60);

        btnLimpiar.setBackground(new java.awt.Color(0, 0, 0));
        btnLimpiar.setFont(new java.awt.Font("Arial Black", 0, 18)); // NOI18N
        btnLimpiar.setForeground(new java.awt.Color(255, 255, 255));
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(140, 750, 160, 30);

        btnCancelar.setBackground(new java.awt.Color(0, 0, 0));
        btnCancelar.setFont(new java.awt.Font("Arial Black", 0, 18)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(340, 750, 160, 30);

        cmbNivel.setEditable(true);
        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1- Licenciatura o Ingenieria", "2- Maestro en Ciencias", "3- Doctor" }));
        getContentPane().add(cmbNivel);
        cmbNivel.setBounds(210, 240, 220, 40);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNumDocenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumDocenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumDocenteActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtDomicilioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDomicilioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDomicilioActionPerformed

    private void txtPagoDiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoDiarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoDiarioActionPerformed

    private void txtHorasTrabajadasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHorasTrabajadasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHorasTrabajadasActionPerformed

    private void txtPagoBonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoBonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoBonoActionPerformed

    private void txtPagoHoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoHoraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoHoraActionPerformed

    private void txtTotalPagar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalPagar1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalPagar1ActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void txtHijosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHijosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHijosActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opc = JOptionPane.showConfirmDialog(this, "Deseas salirte", "Registro", JOptionPane.YES_NO_OPTION);
        
        if (opc == JOptionPane.YES_OPTION){
            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        reg = new Registro();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        boolean exito = false;
        if(this.txtNumDocente.getText().equals(""))exito=true;
        if(this.txtNombre.getText().equals(""))exito=true;
        if(this.txtDomicilio.getText().equals(""))exito=true;
        if(this.txtPagoDiario.getText().equals(""))exito=true;
        if(this.txtHorasTrabajadas.getText().equals(""))exito=true;
        if(this.txtHijos.getText().equals(""))exito=true;
        
        if (exito == true){
            //falto informacion
            JOptionPane.showMessageDialog(this, "Falto capturar informacion");
        }
        else{
            reg.setNumDocente(Integer.parseInt(this.txtNumDocente.getText()));
            reg.setNombre(this.txtNombre.getText());
            reg.setDomicilio(this.txtDomicilio.getText());
            reg.setPagoDiario(Float.parseFloat(this.txtPagoDiario.getText()));
            reg.setHoras(Integer.parseInt(this.txtHorasTrabajadas.getText()));
            setHijos(Integer.parseInt(this.txtHijos.getText()));
            
            switch(this.cmbNivel.getSelectedIndex()){
                case 0:
                    reg.setNivel(0);
                    break;
                case 1:
                    reg.setNivel(1);
                    break;
                case 2:
                    reg.setNivel(2);
                    break;
            }
            JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
            this.btnMostrar.setEnabled(true);
        }
        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        this.txtNumDocente.setText(String.valueOf(reg.getNumDocente()));
        this.txtNombre.setText(reg.getNombre());
        this.txtDomicilio.setText(reg.getDomicilio());
        this.txtPagoDiario.setText(String.valueOf(reg.getPagoDiario()));
        this.txtHorasTrabajadas.setText(String.valueOf(reg.getHoras()));
        
        switch(reg.getNivel()){
            case 0:
                this.cmbNivel.setSelectedIndex(0);
                break;
                
            case 1:
                this.cmbNivel.setSelectedIndex(1);
                break;
            case 2:
                this.cmbNivel.setSelectedIndex(2);
                break;
        }
        
        this.txtPagoHora.setText(String.valueOf(reg.calcularPago()));
        this.txtHijos.setText(String.valueOf(getHijos()));
        this.txtPagoBono.setText(String.valueOf(reg.calcularBono(hijos)));
        this.txtImpuesto.setText(String.valueOf(reg.calcularImpuesto()));
        this.txtTotalPagar1.setText(String.valueOf(reg.calcularTotalPagar(hijos)));
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtHijos;
    private javax.swing.JTextField txtHorasTrabajadas;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumDocente;
    private javax.swing.JTextField txtPagoBono;
    private javax.swing.JTextField txtPagoDiario;
    private javax.swing.JTextField txtPagoHora;
    private javax.swing.JTextField txtTotalPagar1;
    // End of variables declaration//GEN-END:variables
private Registro reg = new Registro();
}
